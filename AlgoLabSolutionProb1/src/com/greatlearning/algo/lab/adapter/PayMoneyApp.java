package com.greatlearning.algo.lab.adapter;

import java.util.Scanner;

public class PayMoneyApp {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        PayMoneyTransactionAdapter moneyTransactionAdapter = new PayMoneyTransactionAdapter();

        System.out.println("Enter the size of transaction array : ");
        int transactionArrSize = scanner.nextInt();

        int[] transactions = new int[transactionArrSize];
        System.out.println("Enter the values of teh array: ");
        for(int i=0; i<transactionArrSize; i++){
            transactions[i] = scanner.nextInt();
        }

        System.out.println("Enter the total number to targets to be achieved : ");
        int noOfTargets = scanner.nextInt();

        for(int i=0; i<noOfTargets; i++){
            System.out.println("Enter the value of the target: ");
            int value = scanner.nextInt();
            int targetAchievedAt = moneyTransactionAdapter.targetAchieved(transactions, value);

            if(targetAchievedAt == -1){
                System.out.println("Target not achieved");
            } else {
                System.out.println("Target achieved after " +targetAchievedAt + " transactions");
            }
        }
        scanner.close();
    }
}
