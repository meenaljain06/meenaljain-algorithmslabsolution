package com.greatlearning.algo.lab.adapter;

public class PayMoneyTransactionAdapter {

    public int targetAchieved(int[] transactions, int targetVal){
        for(int i=0;i<transactions.length; ++i){
            if(transactions[i] >= targetVal){
                return i+1;
            }
            targetVal -= transactions[i];
        }
        return -1;
    }
}
